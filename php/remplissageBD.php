<?php
  date_default_timezone_set('Europe/Paris');
  try{
    //Creation de la base SQLite
    $file_db = new PDO('../../ressources/sqlite:films.sqlite3');
    //Gerer le niveau des erreurs rapportées
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //On insère les réalisateurs

    $file_db->exec("INSERT INTO REALISATEUR values(
      0,
      'Vaughn',
      'Matthew',
      '1971-03-07',
      'Anglais'
    )");

    $file_db->exec("INSERT INTO REALISATEUR values(
      1,
      'Leterrier',
      'Louis',
      '1973-06-17',
      'Français'
    )");

    $file_db->exec("INSERT INTO REALISATEUR values(
      2,
      'Gunn',
      'James',
      '1966-08-05 ',
      'Americains'
    )");

    $file_db->exec("INSERT INTO REALISATEUR values(
      3,
      'Coffin',
      'Pierre',
      '16-03-1967',
      'Français'
    )");


    //On insère maintenant les genres

    $file_db->exec("INSERT INTO GENRE values(
      0,
      'Action'
    )");
    $file_db->exec("INSERT INTO GENRE values(
      1,
      'Comedie'
    )");
    $file_db->exec("INSERT INTO GENRE values(
      2,
      'Thriller'
    )");
    $file_db->exec("INSERT INTO GENRE values(
      3,
      'Policier'
    )");
    $file_db->exec("INSERT INTO GENRE values(
      4,
      'Science'
    )");
    $file_db->exec("INSERT INTO GENRE values(
      5,
      'Fiction'
    )");

    //On insère maintenant les films

    $file_db->exec("INSERT INTO FILMS values(
      0,
      'Kingsman',
      '2015-02-18',
      '../ressources/kingsman.jpg',
      '2h 03 min'
    )");

    $file_db->exec("INSERT INTO FILMS values(
      1,
      'Insaisissable',
      '2013-07-31',
      '../ressources/insaisissable.jpg',
      '2h 04 min'
    )");

    $file_db->exec("INSERT INTO FILMS values(
      2,
      'Les gardiens de la galaxie 2',
      '2017-04-26',
      '../ressources/lgdlg.jpg',
      '2h 05 min'
    )");

    $file_db->exec("INSERT INTO FILMS values(
      3,
      'Les Minions',
      '2015-07-08',
      '../ressources/minion.jpg',
      '2h 06 min'
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      0,
      0
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      1,
      0
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      2,
      1
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      3,
      1
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      4,
      2
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      5,
      2
    )");

    $file_db->exec("INSERT INTO AGENRE values(
      1,
      3
    )");

    $file_db->exec("INSERT INTO AREALISATEUR values(
      0,
      0
    )");
    $file_db->exec("INSERT INTO AREALISATEUR values(
      1,
      1
    )");
    $file_db->exec("INSERT INTO AREALISATEUR values(
      2,
      2
    )");
    $file_db->exec("INSERT INTO AREALISATEUR values(
      3,
      3
    )");
    $file_db->exec("INSERT INTO AREALISATEUR values(
      4,
      3
    )");
  }

  catch(PDOException $e){
  echo $e->getMessage();
}
?>
