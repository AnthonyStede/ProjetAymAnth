<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<title>Movies</title>
		<link rel ="stylesheet" href ="../css/style.css"/>
		<link href="../ressources/index.jpeg" rel="shortcut icon" >
	</head>
	<body>
		<header id="tete">

					<h1> Movie library  </h1>
					<form action="recherche.php" method="get">
						<?php
						echo 'Search <input type="text" name="user_search" placeholder="Search a movie or an actor" value="'.$_GET['user_search'].'" size=50 >';
						?>

						<input type="submit" value="Search">
					</form>
					</form>
					<br/>
		</header>
	</br>
		<section id="ajout">
			<form method="post" action="ajout.php">
				Want to add a movie ?
			<input type="submit" name="button" value="New movie"/>
			</form>
		</br>
		</section>
		<br>
		<section id="liste">
			<h2> List of movies</h2>

			<?php
				include 'affichage.php';
				if ($_GET['user_search'] != "") {
					afficheToutpattern($_GET['user_search']);
				}
				else{
					afficheTout();
				}
			  ?>

		</section>
	</body>
</html>
