<?php
  date_default_timezone_set('Europe/Paris');
  try{
    //Creation de la base SQLite
    $file_db = new PDO('../ressources/sqlite:films.sqlite3');
    //Gerer le niveau des erreurs rapportées
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $file_db->exec("CREATE TABLE REALISATEUR(
      idRea NUMBER,
      nomRea TEXT,
      prenomRea TEXT,
      dateNaissance DATE,
      nationalité TEXT,
      primary key (idRea)
    )");

    $file_db->exec("CREATE TABLE GENRE(
      idGenre NUMBER primary key,
      nomGenre TEXT
    )");

    $file_db->exec("CREATE TABLE FILMS(
      idFilm NUMBER primary key,
      titre TEXT,
      dateFilm DATE,
      affiche TEXT,
      duree TEXT
    )");

    $file_db->exec("CREATE TABLE AGENRE(
      idGenre NUMBER,
      idFilm NUMBER,
      foreign key (idGenre) references GENRE(idGenre),
      foreign key (idFilm) references FILMS(idFilm),
      primary key (idFilm,idGenre)
    )
    ");
    $file_db->exec("CREATE TABLE AREALISATEUR(
      idRea NUMBER,
      idFilm NUMBER,
      foreign key (idRea) references REALISATEUR(idRea),
      foreign key (idFilm) references FILMS(idFilm),
      primary key (idFilm,idRea)
    )
    ");
  }
  catch(PDOException $e){
  echo $e->getMessage();
}
?>
